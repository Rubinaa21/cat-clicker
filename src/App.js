import React, { Component } from "react";
import "./App.css";
import Catlist from "./Catlist";

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Cat clicker App</h1>
        <Catlist />
      </div>
    );
  }
}

export default App;
