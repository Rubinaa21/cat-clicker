import React, { Component } from "react";
import Catlistform from "./Catlistform";
import Modal from "react-modal";
import "./Catcard.css";
import "./Catlist.css";
import uuid from "react-uuid";

class Catlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          name: "Toby",
          clicks: 0,

          imgUrl:
            "https://image.shutterstock.com/image-photo/portrait-surprised-cat-scottish-straight-260nw-499196506.jpg",
          id: uuid(),
        },
        {
          name: "Tommy",
          clicks: 0,

          imgUrl:
            "https://www.humanesociety.org/sites/default/files/styles/1240x698/public/2018/08/kitten-440379.jpg?h=c8d00152&itok=1fdekAh2",
          id: uuid(),
        },
        {
          name: "Snowy",
          clicks: 0,

          imgUrl:
            "https://www.petsworld.in/blog/wp-content/uploads/2014/09/adorable-cat.jpg",
          id: uuid(),
        },
        {
          name: "Pony",
          clicks: 0,

          imgUrl:
            "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFhUVFRUVFxgVFhUVGBUXFhYXGBUVFRUYHSggGBolGxcVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFxAQGisfHR0tLS0tLS0tLS0tLS0rLS0tLSstLTctLS0vLS0tLS0tLy0tLS0tLS0rLSsrLSstLS0rLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA/EAABAwIDBQUGBAUDBAMAAAABAAIRAyEEEjEFQVFhcQYigZGhEzKxwdHwQlJy4QcjYoKSFLLxU6LS4hUWM//EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAApEQEBAAIBAwMDAwUAAAAAAAAAAQIRAxIhMQQiQQUTUTKR8DNxgaHh/9oADAMBAAIRAxEAPwDzKU7U0KTQsWqSkAmCm0JKOGqQamCfMgJAIgQQVMFIxISTBEa1I0YT5UQNUsqAFlSDUXKpBqRg5UsqMWKGVMaIBTAUApSkDkKCcpoQDAJiFKE8JkEWpi1FITIAJamhGIUCggi1NCKmc1MgoSUoTphShKFNoUoQSLWomVSY1bWzsPh/ZOq1i4kOyhrTG6ZJhPGdV0Mr0zbDhOGrScMK8nK59M7s0OaeUiCPVVqtE0yJiDcGzmu6HQp5Y2DHOXwA1qIGqTnNNwMvISR4SZHqnCzXDsYjBqixFSMMlOFItUmsQaICkpFRSB0siQRAgAFqijuahEJg0JKYH39+KRCAGnShWhRp0hnxDoG5gID3/wDiOZ/dVjjb4RllJ5VmUy4w0EngLoj8G8aiOpVLG7ee/u0opM3NZqf1H8XiSgU8Q4GZk8T/AMrecU+WGXNfiLr2RYobgrmL/CTqWifVVsqxymrptjdzYUJEIhYokJBCEynCSYUwnhM1TAQZNKNSkgt43HUIKJTN0Y3V2WU3NA4enmkHVE79MQDLfynTw4HmiVcOZzt0Oo4FXW4ckXC6fLm1pnteD7s82nUdOIUqTwdFHF4BwOYehVTMZknK7juPULLLD8NceSzy1GhEBWfTxZbaoI4EXaehV2m4ESNFlcbG0zlGanlMwpOKnSjgojWITSjNckZi1JqZ1ZukhTBCZGIQnhWWqLRfpf6fJPRbgDtelvLVTa2UnAMaX1DlaPN2lmhc9tHaznktZ3W8Bv4ZjvV48dvlnnyyeGnjNsMpiKcOefxbm/p581hue57pc5x6mfKyhTpknd98VpYbB8T5LeTXaOa23vRcBg811dxOHyxYeK09lYQASQfgq2LrB1Zogw2/WFpO0LyW1BDg23da0W8/mq7VKu4uJJ3qDVx5XdtdeM1JEyEJwU8yg4pGgklKSZK7GoppoVMqw0pKALERjUSFMMSBUHajcbH6rVwdLM3LIzDTmsktVuk+QCd1j8ltxZfDLkx+UnMPuu+irYrZxImCRxVunhmv3keJTVsO4RlqOjgTp5iFrGdjBLHUiRAc06g3HVQOHjvUnEg6sOo5g71qYnDyNTa+63SNyzvYFpBG704H5JJamzqTfxk/TT6qvtOvldDRvg9UfCv46gxzvbx1SqUu8TqbHxgER5J3CWFMrGacY+csXTYnHPEMGrrArTdhDnmJsb+Jj0EqjVwgL2jgDfh9yp+3Ir7mX5UqTC4kHUQrmGpuM3MNjxRaFKKgbGoBNvuFKlOcxYAz4i/31VdKdr+K7lNxi8iL8IBVZmPyD2jhJPut42kTy0Pgli8RNhoQP3/2qhtF2chgtYZo/Lq6Ot/NOyFus3F4upWfLrzp01tyhQr0csDf96rRcImAM1hA3QJ8Gi3VVH3OUEkk950XcdcrB43PmkEsALgAjzC6XA4ZgdJMnxJ+Cw6GCy39OC19nVouLn0Cqdg6N2JYxuY8LD9ty5yg5ri6pHvGB0CtYiqX2c6SbCAhVABYaCwUcuWo148d0JyG4qbihuXM6A3OUC9O5CcUyEzJIGdMglhrEVoTSmzKVJkqbCgqbSg4mSj0KsBwIs4R9Cq0pwES6os3D0KrwCNYNjN1KnjwSQ6+7gqe0qBpPaZs4SCNOiej3tRPPh1+K6nKtPrcCbnebImHw3fuLOEEcDy4gqLSCIMAjfqD1HzRKeI0nUGDxtrbpB81UKoinlJnVvdPMAyPGGk+KuNpBzTeJc0dIgH0Hqq2NfYOm5F+cQNOsFS2TWL3upi2YwCeFzc8LeSuM6034MxA1N+el/gLLJxrYc10bz4SdT4QfFdGantDmAGUNtuJJMQPCPEkrJ7QgNZYC0Ekx4QOd/ROzsIpU4AJjvPgDoAPqoOwhygxYCY3EgXPM/VWsHSLw2bEi3SeO5aGKqA07bwBHC05fQJSBymJeGnXQa6XOnlMqjBku3byd++I8vRWxSzvcT3hmgRPeIkn1m/yUMTS38Y6ADgNwHx8VNglQD5GUWG8nU8ukkoDnFo7o7xkAn8Ld/RGeRlygwTra8eVgqxiDfUxPJIxG1QBBOblMDqT8go//IOFh6Ktm4afD90qdZrTYEnkAgNvY2Ie5ziQLCJ6rRcVT2WyGkxExqj1Xrn5L3dPHPacuQaj0N9VVK1dSu0SpVVd9ZV6lZAdUT0i1b9snVDOknotulhOGp06ybkAnLURjVNzEAFgR6dNRa1WMO0ucGgapydxb2Wq2ED6QBuQeE24SqDmhlogacx1XUvwcU8u8RvjXnC5naNBwmM3P6c12yajit7qOII+nPod6C+wkH74jzUmg72jwMIOJA0BHO5PqkVEOJLm5QL962+4PpqibKxMu5jX+37KrYanvBMfenErSw+GAM3mL+MST8VUSu0MYQYAmTDdTB0b4alQ7SEEspjTuucQPeM92T4+gRMFhnNeHC/0sPgPVNttn8xo4Fo4yG7/AL4p/AWqs06RfliDltoBl3f3DVc7Wxxgybm555hB9Vt7atR943IMHhM6/eq5YVAQfG283F0qFnZeLAGm8xyE3v0/3KOIcahLjZu6bDw/MZ9eOiJRotptLnkWvugncCoMxgMuhzmi5LWuytAH5o+MBGwpOoukwLc/jB+aFVZMzJ+9Fsh1N4AGYgbpaB6RHoq9bCAaNDRv1PnBKWgwanADy+SNQJGtvBWMW2LDz0t4aqgxpkA/fMqQ6rD4gFguTZV61YKphq8tQK1Zc98urG9hatdUqtZDfVQXOTkK1Jz1DMoykqQlmSUUkB1sKYKYlTaJWOnTsWm9FJQg1TaxIzOW52fwsnMRposYroOzroBI6Lbhm8mXLdYt+rTANwIOvQrDx2A7x7uk+PTd971uOIc0OJI1b46zI01QMVTJZmH4Ia6NS38Lh00/xXZpyWuTp7Na8kkEAbhu668F0PYHs1hq2JzOAeykx7nscJBfLAzMNC2HExxAlVK2MaCJvIMGL8L6/NB7N7X/ANBjPbkE0KzDTrASS24iqBvgtuOBPRZcs9lk8iML+IW0sTicRiHNLm4fCvbSysdka0uHdJaDcuyujk081ufw7wjsdhquZs1qDhlcAAXsImDuLhfrvvdaHbbsIca7/UbOq03U67muqAvOUuaCG1AWg3AcQQR812vY3YNHZGELX1BM5qjzAzuIGg4WgDkueZezHHD9Xb/rDHLxHDYnZpa4McD724SIdZpB6X8VW2rg5rXiwAFiLWC6pz21sOx4bcklo35M5yi2trT/AElUdo4ch4J1DRzO43i3gu/pbbcF2oqZWtANpgTuG+3UFcw2vk/mETvHwHxC6XtdS75BkCBAmYEST8VgbYw5bTp8Cb2nQW+ayyG1rEbDr1MPTrm4fTrVokAMpUiATfUku66C8q3/AAx2zVwuPoCmSadWo2lUZPdcHnKHRpmaSDPIjQldN2XrYfaGzBgqjiytSBDSDctzSHAfiHEcuMKz2S7BMwNVmLxddjg0zTa2QJ0BM6uuYaN/guPLmx6MsM537/5/CbdVZ7bdnaLNoltNoaKtJlUMaIaHE1GVCI924YfFy43bey30TfQXGYm3Vd5tWoa9epi6rSwFrGU2vsW0mEubIn3nOJJG4QNy4vb+LD3EwDusN3murixuPHOryu3dcrWrH5dPviosowwu/NYdPxH5eak6l7R5kw1gzPdY5WzFuZMAcSQhVqpcSSIG4XhoGgB3pUJ4V8b9/wAB+6HirFQr2LW8APN3e+BHknxDzACjKNMar5kiVFMpUSdMlKAdJRSQTsSjtKE1PKy06djsepmpZU8yT6iWj2K+ot3YFYwVzRet7YNTN3ZEc1tw+WPLezrMJXBaWkaDMOHd19CT/ar9KnEEMJEQ4D8QOtjfx6LLwByvBDZjrcGxAjlIW65uWxMxEE27p908dIXZHLXI7d2QGXzHKbtcBq3pNt4I1BBXH4itUpkwZHGB56dF6niactLHh3s3GQ4H3Hae0G87pGhHgr+z9l4BxZQq0mPxFNoBdlMEhoJh4jMOqPt2+PhGXLjhrqvl5RsjacHR7TYE03mnJjUhhH3wXYYfZbcTSL8Q6s4Nu0mo55sZMBxMmxHgu8Z2YwAM/wCmpAjeAR6g6K/h6GGpjuMZ/a2dPNTMdfAucczsPZz3Ux7KmQJIa53dAEm4PC504re/+sNfd7zJEd0D5g3R8RtMjRp8UCljHuMmIIK16M7Pwwy9ThLry5jtB/Dhlf3K7muiAXNDx/2wVxG0+wmOo030n0/aU4Aa6j355wRmafCNbrvMftmtTqnLFjEG08DIWjg+2LPdrsc1w3xPjI+ivL0vJJud3Px/U+DLK4261+XkmydnYdjyx1IZmWkiHAxzuP3W2/aJbGRgabwXSSN5IkCPPcvVHbSwdUd4scP6g0/7ll4vbeDp6ezjk0Hyyiyzx48rdTF1X1PFJvqn7vKNo7Tc8HM8u3mLxuAsVhVmOquDGNlxGhIEACXPcdGtGpO5ek9p9s0sWx9AltNoIdTeA7UH8Q6SF5dtnaLcpoYfMKR99x9+ud2cizWDcwdTJ0nlwyw7ZRfDy4cs3hdqONrN/wDzpmWAy50R7Z8RmvowAkNHUm5sDDMzOGazRLjya0Sfp1ISgC29ErtyUhPvVb9KbTb/ACcPJg4rBurF2Zxcd5J801VRpFJ5WeS8fAZUU5TQkoyUpJimRJ1FJAdWzFBQqYsBZTa5jok+rmHNOYwXkq+MeEn4pYryhioRvU3A5yt2niBvXWdk253ZnQ1gtJ0J4NH4jyC4DD1A3vPk7w0GC7mTub6nct3Ye1yXhz3X0aAIa0cABotMMdVOee3qTcWR7ndHG2Y+O4ch6q/RqCqy85qevNhOttYJ8jyWBQxEjvEM/XY/4iXDrC0NnvDHhzS55H4YDWuBEEOMmQQSF0bZLmX+qQfAj6psU7I81aRhxEHdBNyesIlWm1hEmabxLHFxLt0tIBAkGxVB+FecxEOEGBbTWx3wur0t9zzfqn9KWTvL+ytXx9RzifautxJ9Lrotj16ha0ucTOkWEDRcg9ha7+ZTf0gAepXS7Ix7qncDcsDX3o8F28+Ht7eHh+l5tZ+63d+G895Np9LqFN5BEAc0B+DfF6rh0Z8lBuDIuHvPXuj0AXL0zXl6F5Mt/pv+lLaNYB5loN/vcsrFOBJIGUHUT9jei4um4OcXye8YMkW80sBhqTjL3HjGaPU6rqw1jNvN5erPLXjbGLYJA0VTGiBvW37OiBUc8uB72QSD+kErADalRwZ3YJ1MhdGNcuOHfe4x9s1iKbi3XKYj5Li2VIMXnS/zlehbQwzWAnOD8jJtdY1HA16pb/p6rSxxiHUmHITqXVKbXtLLHvl1t8LyPX2XKXb6b6X2xs0xsBh/aElxIp0xnqEbmzAaD+dx7oHE8iqGNxjqry4gAnQDRoFmtHIAAeC6naVRxaKVKKlFl3VWCm72lSO9UcwhwY0Aw1pAIEmRmKzm7PBvkaT/AEyx3+Mlo6NHkuB6rGZSMIbltYmkyNS39QkeJF/RZNajvFxxFwOvDxUVpPABKiSk5RUmdMUkxTBkkkkEvGxlT9nN2qMBKg/K6DoVaEHjcomnF4k7h8OvRX69KYIE6ac1DEVPZnunvRBcPw8mHjxd4DiQKzqIYZruOb/ptjP/AHuIIp9IJ4gI2G206mf5TGU/0g5vGoe8egIHJZ+XgoPagOu2R2hggOHkAPkuwwOLdWga8pgADUkmwA3k2C8v2Xh3El5cGU2mHVHe60m4aBq953NF99gCR0eA2oX/AMumCyjmAk3fVd+EOj3nbwwd1s77uOkyLT0jZO0aTD7FxNRriCXBsspOiA9rTBcLwTaRuNib2OqVXHISSWQbCKWV4tkcAAQ4QbWt5cZRrtDAXaOPcY13erHSzt1OZBqfiMhoiSNbYO3gXNZWLPZNDshDbUiYEUiBLWbt+Y3WnHnccpYy5+OcnFljfmNh+eoBTqANgw1xEf2lw0KsbLoii8sdIfqAQb9CJB6ytHEYEwyoO+xzbkQQWnSY38FGtg3lkN77Ro11ntP9DvqvV+5Mp2vavmLwZcWW7LuNGhj2cQP7gUWpiGEe8J6hc1TxBPdqAkj87TmHWdeqs4VrQ6bHdusssuGTu2w9XllqTWj1oMyWx1b81m1sIPzNFyQSd3h9Fb2jWaAbA79B8FiO2g8AgEtaZFrCN97Lo48brccfqM8d6vdVx2UmGukcpM+MITn5AY7vPeUZtK2YkNbxM3/S3V/oOaNg9gVcU7utc1otf4uOg6fFa55zGd2PFxZZ2SRgYfB1MTVyUw4uHeluWbRucDm6eO5dVS7PPpNhoa+oRFWqGhr3Xn2eaZyWE3INxYLqez/Y4YdwccptMxv4C1wOPPRbdWhxAnjx8V43NljnnuXb6r0eGXFxTHKaryjEdmGzma3IZsRIg8CBcHoPPVDfsEtH8xs8xr4kWPj5r0bGAAkEZTpcWKy9oYloYQWjofko6Y6plXkXaANEtALiNxF/A6+AK5OplmRmYfMeYuPIrpO2NFr3zT0v3d/hx+K5Nzp96/Pf+65s/LbHwLUDtXCf6hHqR87oeXh+6jcXB8Rb/hIvnUeIt+yhRFMn9fimIQCSTJICy6/VTa3juUA7vK3SaCZ8fJaM0mVS0EbyPIfU/DqquK3I1YEyT9jgFWc6bDcgAFXMFgszXVahLaTCA4jV7jcUqc6vIvOjRc7gYYXBmo4NBAm5cdGtF3OPICSrVaKxAafZ0KYIZN+6Lvfl/FUdqfASICDiAc6uWy0im05adOne5uWUxeXGxc8yd5mwOs19PDtz1Q19SC2nRBPs2CSHB7ge82ZzQe+RlzEZ1Tp4wBuYNLKYGVrQe84GYDn8XHMTEQATE5SajKpJNZ8EzDBYDMAIhv5GDL3dPcGkoNpu2g9smoSajxLyYBa02DABYOcIsLNbA4gTZi3Oc1kx+J5H4Q0Ex4NBtxtuWIx8kuNw2975nHSZ1kyTyBU2V4a693WPSZM9SB5FVtLvOzn8QMVh3OHdfSIcRSdo0Na5wa12oFgN/GJXoPZ7t9gcWQ17XYeqd7iDTJ4B4+YC8Iwjr/2vHmwj5qwKsJzOzwnLjxynePpPaOyA8XE8HAwR0PyWLW2fVZo824hoPnlI+C8p2H20xuEAbSrEsH4H99o5CbtHIELrtm/xaraVsNRfOpYXM+OZdGHqspNVwcv07DK7naru02V6hymWj87ok9A0AKxsns7UfDmsk/8AUqkmP0jRa+ze2+EqZS6kGy2dzspBgiSPVdjgcdTqe4QbW6LbL1lmPtxc+H0rG5e/L+f3c5s7sgxrva1nGo7nYeA1+C6VjGRlG7haOgGieoDKGKW9cWfLnyXeVerxen4+GawmlHG169K7f5jeB1Cos7SsdZ9MtPNbvtSLOE8x9FjbYbSiS2J3p4wWq2Oxpc2WtBHPdyK8x7UdqX03GlVoQNxBt4Hd9yul2rjvZg5HW8/TeF5l2g2mapLHxlJsdwPCTp46b7XFZXULGMnGYr2hJkkDX87P1DeOY9NFnYgA+8RJ0dud+r668UUsLTBNxodNLQeB3ckGq4HQAHeNA7mPyu++vNXRFYgtKieScnd/y3keSiAkZwnlRKYJBOUkySAtubNwrLWkN8vqR8FUdU0CtAEgEaLRmh7bcQhU2ifqhvfBuE1NmcxfwQGm4Np0SSb1bQLEsabifwgu14hsKqyXNkyc+4RamwgBrANMzzA5t5pbarzUc0aU/wCUOlPuz4kE+JRndzl7Nrf84hoE785q1PDgEHAMY/M8MbBy90ZdHOMBxHIkAD+lrU2LIs0GQ0ZRzvd3iZPiOChhBBLvytJHX3Wnwc4HwTUtb7hP0QRnGwb4nqf2+adTAUsmqAnh6oF+fodUWmYEqoGolNyDWmItKoRdVnPv0RA7VBtfC4swBO4/Rdv2H7UOpGHO90iZ3t0+S81o1YjoVZo1zJg6q5dJsfT2C2xSqjuuExMTuO9G9s0XmxXzhs3tFWpkQ8h7LA8RwK6HA/xCqgw8hzTuO7oVU6UXqew7QrOHulcptbbxAyvbCy6falrmy1xAO43Cwdr7SNTUDqFVskTJbe4G3sdmk0yOi4TF13Fxlg58+o3ra2gw7rrnKtNwdMnxWVy206VkUw8ZS2Du4HgJ48PLhGRimEGCFrUq0CNVSxr8+vvfH/2+PXWKqKGaddeP1THgkQmlSo0pwkU7Qg0klLKkkEmETfgVdpPhkeHxWfVerTKkDxWjNXxIv971b2UIqNJ0BzHozvR6HyValcq3h/e6Mf4y0j5oDPw1PO9jTfM5rTzkgG6sYzEZv7nOefGzAegk/wB6BgXRUa78pz/4DN8kJrrz98klLrDFN3NzR1gOJH+1RbxUS/8Algf1uP8A2t+qk11k0iU9FPchMKNRcgwmFEYYsmxFO9vuEggCMOiNCrNCOKkoBw2I6fJTaCNOKE11wn9qQfvVAFqzm03nyTM1TDE8Yv6Kba4DhmEjj9Uw0cFiXNMhxH3vG9aorl+4TxabHwKzKdVmrSrtCqBHPUfsmR6hj3mrL2hVbwXS5w8QSOUrJ2tgG5Sfgp6T25n211WxTkWvSANiq1ZyRwFz1BJMkaQRKbVBitUGpHC9mUlcypKVaUqnxQ/aW5py7TkhubdasTtmZCvsHdnfEeCo6FWA7u+JQFTSehHgf2Q0V4BPkouCR7EB7g/U74MU2IANoRm6JlRGlSpm6GwqYKAszJ80qgA6aIRdojPuEGGxSbv6qLFKmblIHfr6J2GYnUT8FEO9CkDfrITB2xN9CiAW4jRMwzE6j7CJTMEjigD4amLED76rTo1C1Z+EqESC0LSwtW3eCqJozcVJ00+9yWIp5gYJVPFBsy10FTbUkRKY+HP7Rpva66zKhlbe0i4WNwsN6i+VREpJwlClSdNqv4ZiosV2g4qaqLnsUkP2pTJKZO9F3pJLVgTvkVM6JJIAIUXpJICKK1JJATYpDVJJAEKNuSSQaLNCpt3pJJAh8ypN1H3uSSTBN39ESrqPFJJAXaGgV2lokkqTVPEao2G3JJJgLbHurl6mqSSnLycMEkklFVBaat00klNXBkkkklP/2Q==",
          id: uuid(),
        },
        {
          name: "Toby",
          clicks: 0,

          imgUrl:
            "https://image.shutterstock.com/image-photo/portrait-surprised-cat-scottish-straight-260nw-499196506.jpg",
          id: uuid(),
        },
        {
          name: "Toby",
          clicks: 0,

          imgUrl:
            "https://image.shutterstock.com/image-photo/portrait-surprised-cat-scottish-straight-260nw-499196506.jpg",
          id: uuid(),
        },
        {
          name: "Toby",
          clicks: 0,

          imgUrl:
            "https://image.shutterstock.com/image-photo/portrait-surprised-cat-scottish-straight-260nw-499196506.jpg",
          id: uuid(),
        },
        {
          name: "Toby",
          clicks: 0,

          imgUrl:
            "https://image.shutterstock.com/image-photo/portrait-surprised-cat-scottish-straight-260nw-499196506.jpg",
          id: uuid(),
        },

        {
          show: false,
        },
      ],
    };
    this.addItem = this.addItem.bind(this);
    this.catCard = this.catCard.bind(this);
    this.cardDisplay = this.cardDisplay.bind(this);
    this.increment = this.increment.bind(this);
  }
  increment = (step) => {
    this.setState({
      clicks: this.state.clicks + step,
    });
  };
  setModalIsOpen = () => {
    this.setState({ show: true });
  };

  modalIsOpen = () => {
    this.setState({ show: false });
  };
  getModal = (value) => {
    this.setState({ show: value });
  };

  addItem(item) {
    let newItem = { ...item, id: uuid() };
    this.setState((state) => ({
      items: [...state.items, newItem],
    }));
  }
  catCard() {
    return (
      <div className="Catcard">
        {this.state.items.map((item) => (
          <div
            onClick={() => this.increment(+1)}
            className="Catcard-info"
            key={item.id}
          >
            <h1 className="Catcard-title">{item.name}</h1>
            <div className="Catcard-image">
              <img src={item.imgUrl} alt={item.name}></img>
            </div>
            <div className="Catcard-data">
              No. of Clicks: {this.state.clicks}
            </div>
          </div>
        ))}
      </div>
    );
  }
  componentWillMount() {
    Modal.setAppElement("body");
  }
  cardDisplay() {
    return (
      <div>
        <button onClick={this.setModalIsOpen}>Open modal</button>

        <Modal
          className="modal"
          isOpen={this.state.show}
          onRequestClose={this.modalIsOpen}
        >
          {this.state.items.map((item) => (
            <div className="modal-info" key={item.id}>
              <h1>{item.name}</h1>
              <div>
                <img
                  className="modal-img"
                  src={item.imgUrl}
                  alt={item.name}
                ></img>
              </div>
              <div>No. of Clicks: {item.clicks}</div>
            </div>
          ))}
          <button onClick={this.modalIsOpen}>Close</button>
        </Modal>
      </div>
    );
  }

  renderItems() {
    return (
      <div className="list-container">
        <ul>
          {this.state.items.map((item) => (
            <li
              onClick={() => this.getModal(item.id)}
              className="Cat-click-list"
              key={item.id}
            >
              <span className="names">{item.name}</span>{" "}
              <span className="clicks">{this.state.clicks}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
  render() {
    return (
      <div className="container">
        <div>
          <Catlistform addItem={this.addItem} />

          {this.renderItems()}
        </div>
        <h1>Image Gallery</h1>
        {this.catCard()}
        <div>{this.cardDisplay()}</div>
      </div>
    );
  }
}
export default Catlist;
