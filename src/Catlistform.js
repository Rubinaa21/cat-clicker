import React, { Component } from "react";
import "./Catform.css";

class Catlistform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      clicks: "",
      imgUrl: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUndo = this.handleUndo.bind(this);
  }
  handleSubmit(evt) {
    evt.preventDefault();
    this.props.addItem(this.state);
    this.setState({
      name: "",
      clicks: "",
      imgUrl: "",
    });
  }
  handleUndo(evt) {
    evt.preventDefault();
    this.setState({ name: "", clicks: "", imgUrl: "" });
  }

  handleChange(evt) {
    this.setState({
      [evt.target.name]: evt.target.value,
    });
  }
  render() {
    return (
      <form className="form-container" onSubmit={this.handleSubmit}>
        <h4>Add A New Cat!</h4>
        <label htmlFor="name">Cat Name:</label>
        <br />
        <input
          id="name"
          name="name"
          value={this.state.name}
          onChange={this.handleChange}
        />
        <br />
        <label htmlFor="imgUrl">Cat Image:</label>
        <br />
        <input
          id="imgUrl"
          name="imgUrl"
          value={this.state.imgUrl}
          onChange={this.handleChange}
        />
        <br />
        <label htmlFor="clicks">Cat Clicks:</label>
        <br />
        <input
          type="number"
          id="clicks"
          name="clicks"
          value={this.state.clicks}
          onChange={this.handleChange}
        />
        <br />
        <button className="save">Save</button>
        <button className="undo" onClick={this.handleUndo}>
          Undo
        </button>
      </form>
    );
  }
}
export default Catlistform;
