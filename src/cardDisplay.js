import React, { useState } from "react";
import Modal from "react-modal";
import Catlist from "./Catlist";

Modal.setAppElement("#root");
function cardDisplay() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    <div className="App">
      <button onClick={() => setModalIsOpen(true)}>Open modal</button>
      <Modal isOpen={modalIsOpen} onRequestClose={() => setModalIsOpen(false)}>
        {this.catCard()}
        <div>
          <button onClick={() => setModalIsOpen(false)}>Close</button>
        </div>
      </Modal>
    </div>
  );
}
export default cardDisplay;
